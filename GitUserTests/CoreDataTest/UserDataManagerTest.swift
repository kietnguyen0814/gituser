//
//  UserDataManagerTest.swift
//  GitUserTests
//
//  Created by Kiet Nguyen on 7/11/22.
//

import XCTest
import Foundation
import CoreData

@testable import GitUser

class UserDataManagerTest: XCTestCase {
    var coreDataStack: CoreDataMockTest!
    var userManager: UserDataManager!
    
    override func setUp() {
        super.setUp()
        coreDataStack = CoreDataMockTest()
        userManager = UserDataManager(mainContext: coreDataStack.mainContext)
    }
    
    func test_create_and_get_new_user() {
        userManager.createNewUser(userId: -1, userName: "test")
        let user = userManager.getUser(-1)
        XCTAssertEqual("test", user?.login)
    }
    
    func test_create_and_get_user_note() {
        userManager.createNewUser(userId: -1, userName: "test")
        guard let user = userManager.getUser(-1) else { return }
        userManager.createNewNoteFromUserId(userId: user.id, note: "note test")
        let note = userManager.getNoteForUserId(Int(user.id))
        XCTAssertEqual("note test", note?.userNote)
    }
}
