//
//  ProfileViewModelTests.swift
//  GitUserTests
//
//  Created by Kiet Nguyen on 7/11/22.
//

import XCTest
@testable import GitUser

class ProfileViewModelTests: BaseUnitTests {
    
    func testGetPresentables() {
        self.profileViewModel?.refresh()
        
        self.profileViewModel?.navBarTitlePresentable.sink(receiveValue: { value in
            XCTAssert(value == "Tom Preston-Werner", "the name is correct")
        }).store(in: &cancellableBag)
        
        self.profileViewModel?.followersPresentable.sink(receiveValue: { value in
            XCTAssert(value == "Followers: 23050", "the followers are correct")
        }).store(in: &cancellableBag)
        
        self.profileViewModel?.followingPresentable.sink(receiveValue: { value in
            XCTAssert(value == "Following: 11", "The following is correct")
        }).store(in: &cancellableBag)
        
        self.profileViewModel?.namePresentable.sink(receiveValue: { value in
            XCTAssert(value == "Name: Tom Preston-Werner", "The name is correct")
        }).store(in: &cancellableBag)
        
        self.profileViewModel?.companyPresentable.sink(receiveValue: { value in
            XCTAssert(value == "Company: @chatterbugapp, @redwoodjs, @preston-werner-ventures ", "The company is correct")
        }).store(in: &cancellableBag)
        
        self.profileViewModel?.blogPresentable.sink(receiveValue: { value in
            XCTAssert(value == "Blog: http://tom.preston-werner.com", "The blog is correct")
        }).store(in: &cancellableBag)
    }
    
    func testSaveNote() {
//        self.profileViewModel?.navBarTitlePresentable.sink(receiveValue: { value in
            self.profileViewModel?.notesPresentable.value = "Note-Test"
            self.profileViewModel?.save()
            self.profileViewModel?.loadOfflineData()
            XCTAssert(self.profileViewModel?.notesPresentable.value == "Note-Test", "The note is saved")
//        }).store(in: &cancellableBag)
    }

    func testLoadDataFailure() {
            let profileViewModel = ProfileViewModel(userId: 1, userLogin: "mojombo", userUseCase: self.failureUsecase, context: self.coreDataStack.mainContext)
            profileViewModel.refresh()
            XCTAssertNil(profileViewModel.userNoteFromAPI, "Users object must be nil.")
    }
    
}
