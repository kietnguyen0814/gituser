//
//  UsersViewModelTests.swift
//  GitUserTests
//
//  Created by Kiet Nguyen on 7/11/22.
//

import XCTest
@testable import GitUser

class UsersViewModelTests: BaseUnitTests {
    func testResultDataSuccess() {
        self.usersViewModel.users.sink(receiveValue: { [weak self] _ in
            DispatchQueue.main.async {
                XCTAssertNotNil(self?.usersViewModel.users, "Users object must not be nil.")
                XCTAssert(self?.usersViewModel.users.value.count ?? 0 > 0, "Users object must be > 0.")
            }
        }).store(in: &cancellableBag)
    }
    
    func testFilterDataSuccess() {
        self.usersViewModel.filterUser(keyword: "mojombo")
        self.usersViewModel.users.sink(receiveValue: { [weak self] _ in
            DispatchQueue.main.async {
                XCTAssert(self?.usersViewModel.users.value.count ?? 0 >= 1, "Results exist")
                XCTAssert(self?.usersViewModel.users.value.first?.login == "mojombo", "User already exists")
            }
        }).store(in: &cancellableBag)
    }
    
    func testResultDataFailure() {
        let viewModel = UsersViewModel(useCase: failureUsecase)
        XCTAssert(viewModel.users.value.count == 0, "No data")
    }
}
