//
//  MockUserRepository.swift
//  GitUserTests
//
//  Created by Kiet Nguyen on 7/11/22.
//

import XCTest
@testable import GitUser

class MockUserRepository: UserRepository {
    var shouldRepoErrorUserList: Bool = false
    var shouldRepoErrorUserProfile: Bool = false
    
    let service: MockUserService = MockUserService()
    
    func getUserList(since userId: Int, completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void) {
        service.shouldServiceErrorUserList = self.shouldRepoErrorUserList
        service.getUserList(since: userId, completion: { result in
            completion(result)
        })
    }
    
    func getUserProfile(for userName: String, completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void) {
        service.shouldServiceErrorUserProfile = self.shouldRepoErrorUserProfile
        service.getUserProfile(for: userName, completion: { result in
            completion(result)
        })
    }
}
