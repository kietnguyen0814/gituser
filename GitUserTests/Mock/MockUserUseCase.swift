//
//  MockUserUseCase.swift
//  GitUserTests
//
//  Created by Kiet Nguyen on 7/11/22.
//

import XCTest
@testable import GitUser

class MockUserUseCase: UserUseCase {
    var shouldUseCaseErrorUserList: Bool = false
    var shouldUseCaseErrorUserProfile: Bool = false
    
    let userRepo: MockUserRepository = MockUserRepository()
    
    func getUserList(since userId: Int, completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void) {
        userRepo.shouldRepoErrorUserList = self.shouldUseCaseErrorUserList
        userRepo.getUserList(since: userId, completion: { result in
            completion(result)
        })
    }
    
    func getUserProfile(for userName: String, completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void) {
        userRepo.shouldRepoErrorUserProfile = self.shouldUseCaseErrorUserProfile
        userRepo.getUserProfile(for: userName, completion: {result in
            completion(result)
        })
    }
}
