//
//  MockUserService.swift
//  GitUserTests
//
//  Created by Kiet Nguyen on 7/11/22.
//

import XCTest
@testable import GitUser

class MockUserService: UserService {
    var shouldServiceErrorUserList: Bool = false
    var shouldServiceErrorUserProfile: Bool = false
    
    func getUserList(since userId: Int, completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void) {
        
        if shouldServiceErrorUserList {
            completion(.failure(.noData))
            return
        }
        
        if let responseData = StubbingUtility.stubbedResponse("Users") {
            do {
                let userList = try JSONDecoder().decode([UserRespone].self, from: responseData)
                completion(.success(userList))

            } catch let error {
                // Failed to decode
                print(error.localizedDescription)
                completion(.failure(.noData))
            }
        }
    }
    
    func getUserProfile(for userName: String, completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void) {
        if shouldServiceErrorUserProfile {
            completion(.failure(.noData))
            return
        }
        
        if let responseData = StubbingUtility.stubbedResponse("GetUser") {
            do {
                let user = try JSONDecoder().decode(UserNoteRespone.self, from: responseData)
                completion(.success(user))

            } catch let error {
                // Failed to decode
                print(error.localizedDescription)
                completion(.failure(.noData))
            }
        }
    }
}
