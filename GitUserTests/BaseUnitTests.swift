//
//  BaseUnitTests.swift
//  GitUserTests
//
//  Created by Kiet Nguyen on 7/11/22.
//

import XCTest
import Combine
@testable import GitUser

class BaseUnitTests: XCTestCase {

    // MARK: - Properties
    var cancellableBag = Set<AnyCancellable>()
    var usersViewModel: UsersViewModel!
    var profileViewModel: ProfileViewModel!
    
    let mockUseCase: MockUserUseCase = MockUserUseCase()
    let failureUsecase: MockUserUseCase = MockUserUseCase()
    
    var coreDataStack: CoreDataMockTest!
    
    
    override func setUp() {
        self.usersViewModel = UsersViewModel(useCase: mockUseCase)
        coreDataStack = CoreDataMockTest()
        
        self.profileViewModel = ProfileViewModel(userId: 1, userLogin: "mojombo", userUseCase: self.mockUseCase, context: self.coreDataStack.mainContext)
        
        mockUseCase.shouldUseCaseErrorUserList = false
        mockUseCase.shouldUseCaseErrorUserProfile = false
        failureUsecase.shouldUseCaseErrorUserList = true
        failureUsecase.shouldUseCaseErrorUserProfile = true
    }
}
