//
//  UserRepository.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

protocol UserRepository {
    func getUserList(since userId: Int, completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void)
    func getUserProfile(for userName: String, completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void)
}

final class GitUserRepositoryImplement: UserRepository {
    let service: UserService

    public convenience init() {
        self.init(service: UserServiceImplement())
    }

    init(service: UserService = UserServiceImplement()) {
        self.service = service
    }
    
    func getUserList(since userId: Int, completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void) {
        self.service.getUserList(since: userId, completion: { result in
            switch result {
            case .success(let listUser):
                completion(.success(listUser))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func getUserProfile(for userName: String, completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void) {
        self.service.getUserProfile(for: userName, completion: { result in
            switch result {
            case .success(let profile):
                completion(.success(profile))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
