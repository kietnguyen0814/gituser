//
//  StubbingUtility.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

struct StubbingUtility {
    /// Helper function for providing file stubbed response.
    static func stubbedResponse(_ filename: String) -> Data! {
        @objc class TestClass: NSObject { }
        
        let bundle = Bundle(for: TestClass.self)
        let path = bundle.path(forResource: filename, ofType: "json")
        return (try? Data(contentsOf: URL(fileURLWithPath: path!)))
    }
}
