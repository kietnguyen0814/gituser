//
//  ConnectionError.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

enum ConnectionError: Swift.Error {
    case invalidURL
    case noData
}
