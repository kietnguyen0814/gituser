//
//  UserService.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

protocol UserService {
    func getUserList(since userId: Int,
                      completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void)

    func getUserProfile(for userName: String,
                         completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void)
}

final class UserServiceImplement: UserService {
    
    private enum Constants: String {
        case baseURL = "https://api.github.com/users"
        
        func userListUrl(since: Int) -> URL? {
            return URL(string: "\(self.rawValue)?since=\(since)")
        }

        func userProfileUrl(for userName: String) -> URL? {
            return URL(string: "\(self.rawValue)/\(userName)")
        }
    }
    
    func getUserList(since userId: Int, completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void) {
        guard let url = Constants.baseURL.userListUrl(since: userId) else {
            assertionFailure("Invalid URL")
            completion(.failure(.invalidURL))
            return
        }

        let request = createRequest(for: url)

        URLSession.shared.dataTask(with: request) { data, _, error in
            guard let data = data, error == nil else {
                // Either no data in response or has an error
                print(error?.localizedDescription ?? "No error")
                completion(.failure(.noData))
                return
            }

            do {
                let listUser = try JSONDecoder().decode([UserRespone].self, from: data)
                completion(.success(listUser))
            } catch let error {
                // Failed to decode
                print(error.localizedDescription)
                completion(.failure(.noData))
            }
        }.resume()
    }
    
    func getUserProfile(for userName: String, completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void) {
        guard let url = Constants.baseURL.userProfileUrl(for: userName) else {
            assertionFailure("Invalid URL")
            completion(.failure(.invalidURL))
            return
        }

        let request = createRequest(for: url)

        URLSession.shared.dataTask(with: request) { data, _, error in
            guard let data = data, error == nil else {
                // Either no data in response or has an error
                print(error?.localizedDescription ?? "No error")
                completion(.failure(.noData))
                return
            }

            do {
                let userProfile = try JSONDecoder().decode(UserNoteRespone.self, from: data)
                print(data.prettyPrintedJSONString as Any)
                completion(.success(userProfile))
            } catch let error {
                // Failed to decode
                print(error.localizedDescription)
                completion(.failure(.noData))
            }
        }.resume()
    }
    
    // MARK: - Private helpers

    private func createRequest(for url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        
        request.httpMethod = "GET"

        return request
    }
    
}

extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}
