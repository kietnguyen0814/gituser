//
//  UserResponse.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

struct UserRespone: Codable {

    let login: String
    let id: Int
    let nodeId: String?
    let avatarUrl: String?
    let gravatarId: String?
    let url: String?
    let htmlUrl: String?
    let followersUrl: String?
    let followingUrl: String?
    let gistsUrl: String?
    let starredUrl: String?
    let subscriptionsUrl: String?
    let organizationsUrl: String?
    let reposUrl: String?
    let eventsUrl: String?
    let receivedEventsUrl: String?
    let type: String?
    let siteAdmin: Bool

    private enum CodingKeys: String, CodingKey {
        case login = "login"
        case id = "id"
        case nodeId = "node_id"
        case avatarUrl = "avatar_url"
        case gravatarId = "gravatar_id"
        case url = "url"
        case htmlUrl = "html_url"
        case followersUrl = "followers_url"
        case followingUrl = "following_url"
        case gistsUrl = "gists_url"
        case starredUrl = "starred_url"
        case subscriptionsUrl = "subscriptions_url"
        case organizationsUrl = "organizations_url"
        case reposUrl = "repos_url"
        case eventsUrl = "events_url"
        case receivedEventsUrl = "received_events_url"
        case type = "type"
        case siteAdmin = "site_admin"
    }
    
    init(from user: User) {
        self.login = user.login ?? ""
        self.id = Int(user.id)
        self.nodeId = user.nodeId
        self.avatarUrl = user.avatarUrl
        self.gravatarId = user.gravatarId
        self.url = user.url
        self.htmlUrl = user.htmlUrl
        self.followersUrl = user.followersUrl
        self.followingUrl = user.followingUrl
        self.gistsUrl = user.gistsUrl
        self.starredUrl = user.starredUrl
        self.subscriptionsUrl = user.subscriptionsUrl
        self.organizationsUrl = user.organizationsUrl
        self.reposUrl = user.reposUrl
        self.eventsUrl = user.eventsUrl
        self.receivedEventsUrl = user.receivedEventsUrl
        self.type = user.type
        self.siteAdmin = user.siteAdmin
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        login = try values.decode(String.self, forKey: .login)
        id = try values.decode(Int.self, forKey: .id)
        nodeId = try values.decode(String.self, forKey: .nodeId)
        avatarUrl = try values.decode(String.self, forKey: .avatarUrl)
        gravatarId = try values.decode(String.self, forKey: .gravatarId)
        url = try values.decode(String.self, forKey: .url)
        htmlUrl = try values.decode(String.self, forKey: .htmlUrl)
        followersUrl = try values.decode(String.self, forKey: .followersUrl)
        followingUrl = try values.decode(String.self, forKey: .followingUrl)
        gistsUrl = try values.decode(String.self, forKey: .gistsUrl)
        starredUrl = try values.decode(String.self, forKey: .starredUrl)
        subscriptionsUrl = try values.decode(String.self, forKey: .subscriptionsUrl)
        organizationsUrl = try values.decode(String.self, forKey: .organizationsUrl)
        reposUrl = try values.decode(String.self, forKey: .reposUrl)
        eventsUrl = try values.decode(String.self, forKey: .eventsUrl)
        receivedEventsUrl = try values.decode(String.self, forKey: .receivedEventsUrl)
        type = try values.decode(String.self, forKey: .type)
        siteAdmin = try values.decode(Bool.self, forKey: .siteAdmin)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(login, forKey: .login)
        try container.encode(id, forKey: .id)
        try container.encode(nodeId, forKey: .nodeId)
        try container.encode(avatarUrl, forKey: .avatarUrl)
        try container.encode(gravatarId, forKey: .gravatarId)
        try container.encode(url, forKey: .url)
        try container.encode(htmlUrl, forKey: .htmlUrl)
        try container.encode(followersUrl, forKey: .followersUrl)
        try container.encode(followingUrl, forKey: .followingUrl)
        try container.encode(gistsUrl, forKey: .gistsUrl)
        try container.encode(starredUrl, forKey: .starredUrl)
        try container.encode(subscriptionsUrl, forKey: .subscriptionsUrl)
        try container.encode(organizationsUrl, forKey: .organizationsUrl)
        try container.encode(reposUrl, forKey: .reposUrl)
        try container.encode(eventsUrl, forKey: .eventsUrl)
        try container.encode(receivedEventsUrl, forKey: .receivedEventsUrl)
        try container.encode(type, forKey: .type)
        try container.encode(siteAdmin, forKey: .siteAdmin)
    }

}
