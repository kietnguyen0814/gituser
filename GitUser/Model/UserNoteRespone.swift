//
//  UserNoteRespone.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

struct UserNoteRespone: Codable {

    let login: String
    let id: Int
    let nodeId: String
    let avatarUrl: String?
    let url: String
    let type: String
    let siteAdmin: Bool
    let name: String?
    let company: String?
    let blog: String?
    let publicRepos: Int
    let publicGists: Int
    let followers: Int
    let following: Int
    let createdAt: String
    let updatedAt: String

    private enum CodingKeys: String, CodingKey {
        case login = "login"
        case id = "id"
        case nodeId = "node_id"
        case avatarUrl = "avatar_url"
        case url = "url"
        case type = "type"
        case siteAdmin = "site_admin"
        case name = "name"
        case company = "company"
        case blog = "blog"
        case publicRepos = "public_repos"
        case publicGists = "public_gists"
        case followers = "followers"
        case following = "following"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        login = try values.decode(String.self, forKey: .login)
        id = try values.decode(Int.self, forKey: .id)
        nodeId = try values.decode(String.self, forKey: .nodeId)
        avatarUrl = try values.decode(String.self, forKey: .avatarUrl)
        url = try values.decode(String.self, forKey: .url)
        type = try values.decode(String.self, forKey: .type)
        siteAdmin = try values.decode(Bool.self, forKey: .siteAdmin)
        name = try values.decode(String.self, forKey: .name)
        company = try values.decode(String.self, forKey: .company)
        blog = try values.decode(String.self, forKey: .blog)
        publicRepos = try values.decode(Int.self, forKey: .publicRepos)
        publicGists = try values.decode(Int.self, forKey: .publicGists)
        followers = try values.decode(Int.self, forKey: .followers)
        following = try values.decode(Int.self, forKey: .following)
        createdAt = try values.decode(String.self, forKey: .createdAt)
        updatedAt = try values.decode(String.self, forKey: .updatedAt)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(login, forKey: .login)
        try container.encode(id, forKey: .id)
        try container.encode(nodeId, forKey: .nodeId)
        try container.encode(avatarUrl, forKey: .avatarUrl)
        try container.encode(url, forKey: .url)
        try container.encode(type, forKey: .type)
        try container.encode(siteAdmin, forKey: .siteAdmin)
        try container.encode(name, forKey: .name)
        try container.encode(company, forKey: .company)
        try container.encode(blog, forKey: .blog)
        try container.encode(publicRepos, forKey: .publicRepos)
        try container.encode(publicGists, forKey: .publicGists)
        try container.encode(followers, forKey: .followers)
        try container.encode(following, forKey: .following)
        try container.encode(createdAt, forKey: .createdAt)
        try container.encode(updatedAt, forKey: .updatedAt)
    }

}
