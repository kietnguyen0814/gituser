//
//  AppNotificationName.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

struct AppNotificationName {
    static let refresh = Notification.Name("refresh")
    static let loadOffline = Notification.Name("loadOffline")
}
