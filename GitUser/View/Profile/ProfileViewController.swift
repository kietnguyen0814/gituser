//
//  ProfileViewController.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Combine
import CombineCocoa
import UIKit

/**
 The controller for profile screen.
 */
class ProfileViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageViewBG: UIImageView!
    @IBOutlet weak var imageViewBanner: UIImageView!
    
    @IBOutlet weak var labelFollowers: UILabel!
    @IBOutlet weak var labelFollowing: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCompany: UILabel!
    @IBOutlet weak var labelBlog: UILabel!
    @IBOutlet weak var textViewNotes: UITextView!
    @IBOutlet weak var buttonSave: UIButton!
    
    @IBOutlet weak var viewShimmerContainer: UIView!
    @IBOutlet var viewShimmers: [UIView]!
    
    var user: UserRespone!
    
    private var viewModel: ProfileViewModel!
//    private let disposeBag = DisposeBag()
    private(set) var cancellableBag = Set<AnyCancellable>()
    
    // MARK: - Functions
    
    private func setupBindings() {
        
        self.viewModel = ProfileViewModel(userId: user.id, userLogin: user.login, userUseCase: UserUseCaseImplement(), context: DataManager.shared.mainContext)
        
        self.viewModel.navBarTitlePresentable
            .assign(to: \.title, on: self.navigationItem)
            .store(in: &cancellableBag)
        
        self.viewModel.followersPresentable
            .assign(to: \.text, on: self.labelFollowers)
            .store(in: &cancellableBag)
        
        self.viewModel.followingPresentable
            .assign(to: \.text, on: self.labelFollowing)
            .store(in: &cancellableBag)
        
        self.viewModel.namePresentable
            .assign(to: \.text, on: self.labelName)
            .store(in: &cancellableBag)
        
        self.viewModel.companyPresentable
            .assign(to: \.text, on: self.labelCompany)
            .store(in: &cancellableBag)
        
        self.viewModel.blogPresentable
            .assign(to: \.text, on: self.labelBlog)
            .store(in: &cancellableBag)
        
        self.viewModel.namePresentable
            .assign(to: \.text, on: self.labelName)
            .store(in: &cancellableBag)
        
        self.viewModel.notesPresentable
            .assign(to: \.text, on: self.textViewNotes)
            .store(in: &cancellableBag)
        
        self.textViewNotes.textPublisher
            .assign(to: \.notesPresentable.value, on: self.viewModel)
            .store(in: &cancellableBag)
        
        self.viewModel.imageBanner
            .sink(receiveValue: { [weak self] imageBanner in
                DispatchQueue.main.async {
                    self?.imageViewBG.image = imageBanner
                    self?.imageViewBanner.image = imageBanner
                }
            })
            .store(in: &cancellableBag)
        
        self.viewModel.startShimmer
            .sink(receiveValue: { [weak self] startShimmer in
                DispatchQueue.main.async {
                    self?.viewShimmerContainer.isHidden = !startShimmer
                    if startShimmer {
                        self?.viewShimmers.forEach({ $0.shimmer() })
                    }
                }
            })
            .store(in: &cancellableBag)
        
        self.buttonSave.tapPublisher
            .sink { [weak self] _ in
                self?.view.endEditing(true)
                self?.viewModel.save()
            }
            .store(in: &cancellableBag)
        
    }
    
    private func setupUI() {
        self.navigationItem.largeTitleDisplayMode = .never
        self.scrollView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        self.imageViewBG.addSubview(blurredEffectView)
        blurredEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            blurredEffectView.topAnchor.constraint(equalTo: self.imageViewBG.topAnchor),
            blurredEffectView.bottomAnchor.constraint(equalTo: self.imageViewBG.bottomAnchor),
            blurredEffectView.leadingAnchor.constraint(equalTo: self.imageViewBG.leadingAnchor),
            blurredEffectView.trailingAnchor.constraint(equalTo: self.imageViewBG.trailingAnchor)
        ])
        
        self.textViewNotes.setupLayer(
            cornerRadius: 4.0,
            borderWidth: 1.0,
            borderColor: .textColor
        )
    }
    
    // MARK: Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.setupBindings()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.viewModel.viewWillDisappear()
    }
}
