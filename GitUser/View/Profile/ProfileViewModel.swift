//
//  ProfileViewModel.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation
import UIKit
import Combine
import CoreData

class ProfileViewModel: BaseViewModel {
    
    // MARK: - Properties
    private var userLocal: User?
    private var note: Note?
    private var userId: Int
    private var userName: String
    private var context: NSManagedObjectContext
    
    var userNoteFromAPI: UserNoteRespone?
    
    private let useCase: UserUseCase
    
    // MARK: Outputs
    
    /// Determines the state of shimmer
    var startShimmer = CurrentValueSubject<Bool, Never>(true)
    var imageBanner = CurrentValueSubject<UIImage?, Never>(nil)
    
    var navBarTitlePresentable = CurrentValueSubject<String?, Never>("")
    var followersPresentable = CurrentValueSubject<String?, Never>("Followers")
    var followingPresentable = CurrentValueSubject<String?, Never>("Following")
    var namePresentable = CurrentValueSubject<String?, Never>("Name: ")
    var companyPresentable = CurrentValueSubject<String?, Never>("Company: ")
    var blogPresentable = CurrentValueSubject<String?, Never>("Blog: ")
    var notesPresentable = CurrentValueSubject<String?, Never>("Notes...")
    
    private var cancellable: AnyCancellable?
    
    
    
    // MARK: - Functions
    
    /// Put the data to the behavior relays.
    private func getPresentables() {
        guard let userNote = userNoteFromAPI else { return }
        
        self.navBarTitlePresentable.send(userNote.name ?? "")
        self.followersPresentable.send("Followers: \(userNote.followers)")
        self.followingPresentable.send("Following: \(userNote.following)")
        self.namePresentable.send("Name: \(userNote.name ?? "")")
        self.companyPresentable.send("Company: \(userNote.company ?? "")")
        self.blogPresentable.send("Blog: \(userNote.blog ?? "")")
        
        // Image Loader...
        let imageLoader = ImageLoader.shared.loadImage(from: URL(string: userNote.avatarUrl ?? "")!)
        self.cancellable = imageLoader.sink { [weak self] image in
            self?.imageBanner.send(image)
        }
        
        // Note from different entity.
        let note = self.note?.userNote ?? ""
        self.notesPresentable.send(note)
    }
    
    /// Fetch objects from Core Data for offline mode.
    override func loadOfflineData() {
        self.userLocal = UserDataManager(mainContext: self.context).getUser(Int(self.userId))
        self.note = UserDataManager(mainContext: self.context).getNoteForUserId(self.userId)
        self.getPresentables()
    }
    
    /// Call API to get more user data
    private func loadData() {
        self.startShimmer.send(true)
        let username = self.userName
        self.loadOfflineData()
        self.useCase.getUserProfile(for: username, completion: { [weak self] result in
            guard let `self` = self else { return }
            self.startShimmer.send(false)
            switch result {
            case .success(let userNote):
                self.userLocal?.followers = Int32(userNote.followers)
                self.userLocal?.following = Int32(userNote.following)
                self.userLocal?.name = userNote.name
                self.userLocal?.company = userNote.company
                self.userLocal?.blog = userNote.blog
                DataManager.shared.saveContext(self.context)
                self.getPresentables()
                self.userNoteFromAPI = userNote
                DispatchQueue.main.async {
                    self.getPresentables()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    self.showError(error)
                }
            }
        })
    }
    
    // MARK: Events
    
    func save() {
        if self.note == nil {
            let newNote = Note(context: self.context)
            newNote.userId = Int32(self.userId)
            newNote.userNote = self.notesPresentable.value
            self.note = newNote
        } else {
            self.note?.userId = Int32(self.userId)
            self.note?.userNote = self.notesPresentable.value
        }
        
        DataManager.shared.saveContext(self.context)
        
        self.showAlert("Saved!")
    }
    
    // MARK: Overrides
    
    override func refresh() {
        self.loadData()
    }
    
    init(userId: Int, userLogin: String, userUseCase: UserUseCase, context: NSManagedObjectContext!) {
        self.userId = userId
        self.userName = userLogin
        self.useCase = userUseCase
        self.context = context
        super.init()
        self.loadData()
    }
    
    func viewWillDisappear() {
        NotificationCenter.default.removeObserver(self)
        self.cancellable?.cancel()
    }
}
