//
//  BaseViewModel.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation
import UIKit

/**
 The Base ViewModel of all ViewModels of the app.
 This contains the common properties of all viewModels.
 */
class BaseViewModel: NSObject {
    
    // MARK: - Properties
    
    // MARK: - Functions
    
    @objc func loadOfflineData() { }
    @objc func refresh() { }
    
    func showError(_ error: Error) {
        if (error as NSError).code == -1009 { return }
        UIViewController.current()?.alert(
            title: "Oops!",
            message: "An error has occured: \(error.localizedDescription)",
            okayButtonTitle: "OK",
            cancelButtonTitle: nil,
            withBlock: nil
        )
    }
    
    func showAlert(_ message: String) {
        UIViewController.current()?.alert(
            title: message,
            message: nil,
            okayButtonTitle: "OK",
            cancelButtonTitle: nil,
            withBlock: nil
        )
    }
    
    override init() {
        super.init()
        
        // Be notified from internet status.
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.refresh),
            name: AppNotificationName.refresh,
            object: nil
        )
    }
}
