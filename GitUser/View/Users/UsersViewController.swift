//
//  UsersViewController.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import UIKit
import Combine
import CombineCocoa

class UsersViewController: BaseViewController {
    
    // MARK: - Properties
    
    var viewModel: UsersViewModel!
    private(set) var cancellableBag = Set<AnyCancellable>()
    private var searchController: UISearchController!
    
    // MARK: - Functions
    
    private func setupBindings() {
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.searchController.searchResultsUpdater = self
        
        self.viewModel.loaderIsHidden
            .assign(to: \.isHidden, on: self.activityIndicatorContainerView)
            .store(in: &cancellableBag)
        
        self.refreshControl.isRefreshingPublisher
            .map { [weak self] _ in
                self?.refreshControl.isRefreshing == false
            }
            .filter { $0 == false }
            .sink(receiveValue: { [weak self] _ in
                self?.viewModel.refresh()
            })
            .store(in: &cancellableBag)
        
        self.refreshControl.isRefreshingPublisher
            .map { [weak self] _ in
                self?.refreshControl.isRefreshing == true
            }
            .filter { $0 == true }
            .sink(receiveValue: { _ in
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) { [weak self] in
                    self?.refreshControl.endRefreshing()
                }
            })
            .store(in: &cancellableBag)
        
        self.viewModel.users.sink(receiveValue: { [weak self] _ in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }).store(in: &cancellableBag)
        
        self.viewModel.filteredUsers.sink(receiveValue: { [weak self] _ in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }).store(in: &cancellableBag)
    }
    
    private func setupUI() {
        self.title = "Users"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        self.view.addSubview(self.tableView)
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
        ])
        
        self.addPullToRefreshControl(to: self.tableView)
        
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.navigationItem.searchController = self.searchController
    }
    
    // MARK: Overrides
    
    override func loadView() {
        super.loadView()
        
        self.viewModel = UsersViewModel(useCase: UserUseCaseImplement())
        self.setupUI()
        self.setupBindings()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.largeTitleDisplayMode = .automatic
    }
}

extension UsersViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.viewModel.filtering {
            if self.viewModel.filteredUsers.value.count > indexPath.row {
                let user = self.viewModel.filteredUsers.value[indexPath.row]
                self.showProfile(for: user)
            }
        } else {
            if self.viewModel.users.value.count > indexPath.row {
                let user = self.viewModel.users.value[indexPath.row]
                self.showProfile(for: user)
            }
        }
    }
}

extension UsersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UserTableViewCell?
        
        cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.identifier) as? UserTableViewCell
        
        if cell == nil {
            cell = UserTableViewCell()
        }
        
        let row = indexPath.row
        
        if self.viewModel.filtering {
            if self.viewModel.filteredUsers.value.count > row {
                let shouldInvert = (row + 1) % 4 == 0 && row > 0
                let user = self.viewModel.filteredUsers.value[row]
                cell?.configure(with: user, invert: shouldInvert)
            }
        } else {
            if self.viewModel.users.value.count > row {
                let shouldInvert = (row + 1) % 4 == 0 && row > 0
                let user = self.viewModel.users.value[row]
                cell?.configure(with: user, invert: shouldInvert)
            }
        }
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.filtering ? self.viewModel.filteredUsers.value.count : self.viewModel.users.value.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let reachability = try? Reachability(), reachability.connection == .unavailable {
            return
        }
        
        let lastItem = self.viewModel.users.value.count - 1
        if indexPath.row == lastItem {
            tableView.tableFooterView = self.activityIndicatorContainerView
            self.viewModel.loadMoreUsers()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                tableView.tableFooterView = nil
            }
        }
    }
}

extension UsersViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text != "" {
            self.viewModel.filterUser(keyword: searchController.searchBar.text)
        }
    }
}

extension UsersViewController {
    func showProfile(for user: UserRespone) {
        // TODO: Use coordinator or router.
        let profileController = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
        profileController.user = user
        self.navigationController?.pushViewController(profileController, animated: true)
    }
}
