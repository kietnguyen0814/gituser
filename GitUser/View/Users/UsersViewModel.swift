//
//  UsersViewModel.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation
import UIKit
import CoreData
import Combine

/**
 The viewmodel that the `UsersViewController` owns.
 */
class UsersViewModel: BaseViewModel {
    
    // MARK: - Properties
    
    private var queue = OperationQueue()
    
    /// The last seen user id.
    private var since: Int = 0
    
    private let userUseCase: UserUseCase
    
    var users = CurrentValueSubject<[UserRespone], Never>([])
    var usersLocal: [User] = [User]()
    
    var filteredUsers = CurrentValueSubject<[UserRespone], Never>([])
    var filtering: Bool = false
    
    // MARK: Visibilities
    
    /// Should the loader be hidden?
    var loaderIsHidden = CurrentValueSubject<Bool, Never>(true)
    /// Should the tableView be hidden? I.e. hidden whilst loader is visible
    var tableViewIsHidden = CurrentValueSubject<Bool, Never>(true)
    
    
    init(useCase: UserUseCase) {
        self.userUseCase = useCase
        super.init()
        self.queue.maxConcurrentOperationCount = 1
        self.loadUsers()
        
        // Be notified from internet status.
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.refresh),
            name: AppNotificationName.refresh,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.loadOfflineData),
            name: AppNotificationName.loadOffline,
            object: nil
        )
    }
    
    // MARK: - Functions
    
    /// Function to re-do searching. Called by the refresh control
    override func refresh() {
        self.loadUsers(since: self.since)
    }
    
    func clearStorage() {
        let managedObjectContext = DataManager.shared.mainContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedObjectContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print(error)
        }
    }
    
    /// Fetch objects from Core Data for offline mode.
    override func loadOfflineData() {
        if self.users.value.count > 0 { return }
        
        let managedObjectContext = DataManager.shared.mainContext
        let fetchRequest = NSFetchRequest<User>(entityName: "User")
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            let users = try managedObjectContext.fetch(fetchRequest)
            self.usersLocal = users
            self.users.value = self.usersLocal.map {UserRespone(from: $0)}
            DispatchQueue.main.async {
                self.loaderIsHidden.send(true)
                self.tableViewIsHidden.send(false)
            }
        } catch let error {
            print(error)
        }
    }
    
    func loadMoreUsers() {
        self.loadUsers(since: self.since)
    }
    
    private func loadUsers(since: Int = 0) {
        if since == 0 {
            self.loaderIsHidden.send(false)
            self.tableViewIsHidden.send(true)
        }
        
        self.queue.cancelAllOperations()
        self.queue.qualityOfService = .background
        
        let block = BlockOperation {
            self.userUseCase.getUserList(since: self.since, completion: { [weak self] result in
                switch result {
                case .success(let listUser):
                    if since == 0 {
                        self?.clearStorage()
                        self?.users.send([])
                    }
                    
                    self?.users.value.append(contentsOf: listUser)
                    
                    if let lastUser = listUser.last {
                        self?.since = Int(lastUser.id)
                        print("LOG: Got new users from API: COUNT \(listUser.count). LAST ID: \(self?.since.description ?? "")")
                    }
                    
                    //Save local data
                    do {
                        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
                            DispatchQueue.main.async {
                                let error = NSError(domain: "Error Core Data", code: 0, userInfo: nil)
                                self?.showError(error)
                            }
                            return
                        }
                        let managedObjectContext = DataManager.shared.mainContext
                        let data = try JSONEncoder().encode(listUser)
                        let decoder = JSONDecoder()
                        decoder.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
                        let result = try decoder.decode([User].self, from: data)
                        self?.usersLocal.append(contentsOf: result)
                        DataManager.shared.saveContext(DataManager.shared.mainContext)
                    } catch let err {
                        print("Failed to encode with error \(err)")
                    }
                    DispatchQueue.main.async {
                        self?.loaderIsHidden.send(true)
                        self?.tableViewIsHidden.send(false)
                    }
                case .failure(let error):
                    DispatchQueue.main.async {
                        self?.showError(error)
                    }
                }
            })
        }

        self.queue.addOperation(block)
        
        
    }
    
    func filterUser(keyword string: String?) {
        if let text = string, !text.isEmpty {
            self.filteredUsers.value = self.users.value.filter({ (user) -> Bool in
                let loginContains = user.login.lowercased().contains(text.lowercased())
                return loginContains
            })
            self.filtering = true
        } else {
            self.filtering = false
            self.filteredUsers.value = []
        }
    }
}
