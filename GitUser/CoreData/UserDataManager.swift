//
//  UserDataManager.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation
import CoreData

final class UserDataManager {
    // MARK: Contexts
    
    let mainContext: NSManagedObjectContext
    
    // MARK: - Init
    
    init(mainContext: NSManagedObjectContext = DataManager.shared.mainContext) {
        self.mainContext = mainContext
    }
    
    func createNewUser(userId: Int32, userName: String) {
        mainContext.performAndWait {
            let user: User = User(context: mainContext)
            user.id = userId
            user.login = userName
            try? mainContext.save()
        }
    }
    
    /// Fetch the specific locally stored user.
    func getUser(_ userId: Int) -> User? {
        let managedObjectContext = mainContext
        let fetchRequest = NSFetchRequest<User>(entityName: "User")
        
        let userid = "\(userId)"
        let predicate = NSPredicate(format: "id == \(userid)")
        fetchRequest.predicate = predicate
        
        do {
            let users = try managedObjectContext.fetch(fetchRequest)
            return users.first
        } catch let error {
            print(error)
            return nil
        }
    }
    
    func createNewNoteFromUserId(userId: Int32, note: String) {
        mainContext.performAndWait {
            let newNote = Note(context: mainContext)
            newNote.userId = userId
            newNote.userNote = note
            try? mainContext.save()
        }
    }
    
    /// Fetch the separated entity note for the specific user.
    func getNoteForUserId(_ userId: Int) -> Note? {
        let managedObjectContext = mainContext
        let fetchRequest = NSFetchRequest<Note>(entityName: "Note")
        
        let userid = "\(userId)"
        let predicate = NSPredicate(format: "userId == \(userid)")
        fetchRequest.predicate = predicate
        
        do {
            let notes = try managedObjectContext.fetch(fetchRequest)
            return notes.first
        } catch let error {
            print(error)
            return nil
        }
    }
}
