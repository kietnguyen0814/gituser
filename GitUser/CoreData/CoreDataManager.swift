//
//  CoreDataManager.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import CoreData

protocol CoreDataStackProtocol {
    func saveContext(_ context: NSManagedObjectContext)
}

/**
 The singleton that handles the Core Data stack.
 */
final class DataManager {
    
    // MARK: - Properties
    
    static let shared = DataManager()
    
    ///Returns the default managed object context of application linked to the main queue.
    var mainContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "GitUser")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        
        container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        container.viewContext.undoManager = nil
        container.viewContext.shouldDeleteInaccessibleFaults = true
        
        container.viewContext.automaticallyMergesChangesFromParent = true
        
        return container
    }()
    
    // MARK: Init
    
    private init() {}
    
}

extension DataManager: CoreDataStackProtocol {
    ///Saves the context to the persistent store.
    func saveContext(_ context: NSManagedObjectContext) {
        context.performAndWait {
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
    }
}
