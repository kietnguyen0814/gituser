//
//  UserUseCase.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

protocol UserUseCase {
    func getUserList(since userId: Int, completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void)
    func getUserProfile(for userName: String, completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void)
}

final class UserUseCaseImplement: UserUseCase {
    
    private let userRepository: UserRepository

    init(userRepository: UserRepository = GitUserRepositoryImplement()) {
        self.userRepository = userRepository
    }
    
    func getUserList(since userId: Int, completion: @escaping (Result<[UserRespone], ConnectionError>) -> Void) {
        self.userRepository.getUserList(since: userId, completion: { result in
            switch result {
            case .success(let listUser):
                completion(.success(listUser))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
    
    func getUserProfile(for userName: String, completion: @escaping (Result<UserNoteRespone, ConnectionError>) -> Void) {
        self.userRepository.getUserProfile(for: userName, completion: { result in
            switch result {
            case .success(let profile):
                completion(.success(profile))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
