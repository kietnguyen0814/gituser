//
//  CodingUserInfoKeyExt.swift
//  GitUser
//
//  Created by Kiet Nguyen on 7/11/22.
//

import Foundation

public extension CodingUserInfoKey {
    // Helper property to retrieve the context
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")
}

