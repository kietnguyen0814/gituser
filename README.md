# GitUser

The GitUser app gets and shows list git user

# tawk.to Bonus

- [x] Skeleton is added only on user details screen.

- [x] Any data fetch should utilize Result types

- [x] All CoreData write queries must be queued while allowing one concurrent query at any time

- [x] The project base on MVVM & Coordinator design pattern.

- [x] Users list UI is done in code and Profile with Interface Builder.

- [x] CoreData write queries must be queued while allowing one concurrent query at
any time

- [x] App supports dark mode.